:#!/bin/sh

sudo apt --yes purge neovim neovim-runtime python3 python3-venv sqlite

## Install latest version from Github
sudo rm -rf /opt/nvim/ || true
wget -N https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz || true
sudo tar xvzf nvim-linux64.tar.gz -C /opt/
sudo mv /opt/nvim-linux64 /opt/nvim

sudo ln -fs /opt/nvim/bin/nvim /usr/bin/nvim

## Install plugins
sh -c 'curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

echo "After: Remember to run :PlugInstall"
