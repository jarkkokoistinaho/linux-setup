#!/bin/sh
set -x

# OH MY ZSH
if [ ! -d ~/.oh-my-zsh/ ]; then
  echo "Oh My ZSH not found! Installing..."
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  REBOOT=true
else
  echo "Oh My ZSH found from home folder."
fi

# Backup old configuration
cp ~/.zshrc ~/.zshrc-backup-`date +%s`

## Install plugins
OLD='plugins=(git.*)'
PLUGINS="plugins=(git)"

install_plugin() {
    PLUGIN_NAME=$1
    echo "Installing $PLUGIN_NAME"
    ls ~/.oh-my-zsh/custom/plugins | grep -q $PLUGIN_NAME || git clone https://github.com/zsh-users/$PLUGIN_NAME ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/$PLUGIN_NAME
    PLUGINS=`echo $PLUGINS | sed "s|)| $PLUGIN_NAME)|"`
}

install_plugin "zsh-autosuggestions"
install_plugin "zsh-history-substring-search"
install_plugin "zsh-syntax-highlighting"

sed -i "s|${OLD}|${PLUGINS}|" ~/.zshrc

