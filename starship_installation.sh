#!/bin/sh

if ! grep -q "starship init zsh" ~/.zshrc ; then
  echo "Starship not found! Installing..."
  sh -c "$(curl -fsSL https://starship.rs/install.sh)"
  echo 'eval "$(starship init zsh)"' >> ~/.zshrc
fi
 
