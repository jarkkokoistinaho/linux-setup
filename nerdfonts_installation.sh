#!/bin/sh

# Install Nerd Fonts
# wget -N https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip
# unzip FiraCode.zip -d FiraCode
# mkdir -p ~/.local/share/fonts/ && mv ~/FiraCode/* ~/.local/share/fonts/
# fc-cache -fv


# Install FiraCode
sudo apt --yes install fonts-firacode

# Install Nerd Fonts Symbols
wget -N https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/NerdFontsSymbolsOnly.zip
unzip NerdFontsSymbolsOnly.zip -d NerdFontsSymbolsOnly
mkdir -p ~/.local/share/fonts/ && mv ~/NerdFontsSymbolsOnly/* ~/.local/share/fonts/
fc-cache -fv

