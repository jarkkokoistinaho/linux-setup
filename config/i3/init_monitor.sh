#!/bin/sh

MONITORS=`xrandr | grep -w "connected"`
MONITORS_COUNT=`echo "$MONITORS" | wc -l`
if [ $MONITORS_COUNT -eq 1 ]; then
  xrandr --output eDP --auto --primary
elif [ $MONITORS_COUNT -eq 2 ]; then
  EXT_MONITOR=`echo "$MONITORS" | awk '{print $1}' | grep -v "eDP"`
  xrandr --output $EXT_MONITOR --auto --primary --left-of eDP
fi
