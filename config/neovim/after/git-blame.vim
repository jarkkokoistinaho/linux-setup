
let g:gitblame_message_template = '<author> • <summary> (<date>)'
let g:gitblame_date_format = '%d.%m.%Y %H:%M'
