local lspconfig = require('lspconfig')
lspconfig.tsserver.setup {}
lspconfig.rust_analyzer.setup {}
lspconfig.docker_compose_language_service.setup {}
lspconfig.dockerls.setup {}
lspconfig.terraformls.setup {}
lspconfig.pylsp.setup {}

cfg = { }  -- add you config here
require "lsp_signature".setup(cfg)
