""" Plugins
call plug#begin(stdpath('data') . '/plugged')
Plug 'neovim/nvim-lspconfig'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update

Plug 'folke/tokyonight.nvim', { 'branch': 'main' }

Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdtree'

Plug 'mhinz/vim-startify'

""" Awesome Neovim https://github.com/rockerBOO/awesome-neovim
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
" 9000+ Snippets
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}

Plug 'liuchengxu/vim-which-key'
set timeoutlen=500
Plug 'b0o/mapx.nvim'

Plug 'seandewar/killersheep.nvim'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.0' }

Plug 'kyazdani42/nvim-web-devicons'
Plug 'romgrk/barbar.nvim'

Plug 'seandewar/nvimesweeper'

Plug 'numToStr/Comment.nvim'
Plug 'ray-x/lsp_signature.nvim'

Plug 'jubnzv/virtual-types.nvim'

Plug 'f-person/git-blame.nvim'

Plug 'jubnzv/virtual-types.nvim'

Plug 'nvim-lualine/lualine.nvim'

Plug 'anuvyklack/windows.nvim'

Plug 'anuvyklack/middleclass'

Plug 'anuvyklack/animation.nvim'

Plug 'kylechui/nvim-surround'

Plug 'piersolenski/wtf.nvim'
Plug 'MunifTanjim/nui.nvim'

call plug#end()

