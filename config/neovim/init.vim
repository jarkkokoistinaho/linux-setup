""" Neovim init
source ${HOME}/.config/nvim/plug.vim
source ${HOME}/.config/nvim/keys/whichkey.vim
source ${HOME}/.config/nvim/keys/keymaps.lua

source ${HOME}/.config/nvim/after/treesitter.lua
source ${HOME}/.config/nvim/after/comment.lua
source ${HOME}/.config/nvim/after/lspconfig.lua
source ${HOME}/.config/nvim/after/git-blame.vim
source ${HOME}/.config/nvim/after/virtual-types.lua
source ${HOME}/.config/nvim/after/lualine.lua
source ${HOME}/.config/nvim/after/windows.lua
source ${HOME}/.config/nvim/after/simple_activations.lua

""" Main Configurations
set showmatch               " show matching 
set ignorecase              " case insensitive 
set mouse=v                 " middle-click paste with 
set hlsearch                " highlight search 
set incsearch               " incremental search
set tabstop=2               " number of columns occupied by a tab 
set softtabstop=2           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=2            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
set number                  " add line numbers
filetype plugin indent on   " allow auto-indenting depending on file type
syntax on                   " syntax highlighting
set mouse=a                 " enable mouse click
set clipboard=unnamedplus   " using system clipboard
set wildmode=longest,list   " get bash-like tab completions
set colorcolumn=80          " set an 80 column border for good coding style
set cursorline

""" Colorscheme
let g:tokyonight_style = "night"
colorscheme tokyonight
