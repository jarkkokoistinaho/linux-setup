-- Enable Mapx
require'mapx'.setup{ global = true }

-- Alt - Up/Down -- move lines up/down
nnoremap("<A-Down>", ":m .+1<CR>==")
nnoremap("<A-Up>", ":m .-2<CR>==")
inoremap("<A-Down>", "<Esc>:m .+1<CR>==gi")
inoremap("<A-Up>", "<Esc>:m .-2<CR>==gi")
vnoremap("<A-Down>", ":m '>+1<CR>gv=gv")
vnoremap("<A-Up>", ":m '<-2<CR>gv=gv")

-- Insert mode - Text selection
inoremap("<S-Right>", "<C-o>v<S-Right>")
inoremap("<S-Left>", "<Left><C-o>v<S-Left>")
inoremap("<S-Down>", "<C-o>v<Down>")
inoremap("<S-Up>", "<Left><C-o>v<Up>")

-- Code (Jump / Go to)
nnoremap("<F3>", ":lua vim.lsp.buf.definition()<CR>")
inoremap("<F3>", ":lua vim.lsp.buf.definition()<CR>")
vnoremap("<F3>", ":lua vim.lsp.buf.definition()<CR>")

-- Code (Format)
nnoremap("<C-C><C-F>", ":lua vim.lsp.buf.format()<CR>")
inoremap("<C-C><C-F>", ":lua vim.lsp.buf.format()<CR>")
vnoremap("<C-C><C-F>", ":lua vim.lsp.buf.format()<CR>")


