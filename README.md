# Linux Setup

This contains installation scripts for quick setuping Linux-based machine.

## Quick installation
Just run: `sh setup.sh`

## What does it install?

- curl
- zsh
- git
- tmux
- oh-my-zsh, with plugins:
    - zsh-autosuggestions
    - zsh-history-substring-search
    - zsh-syntax-highlighting
- nerdfonts
- starship
- neovim, with plugins:
    - vim-plug
    - lspconfig
    - treesitter
    - tokyonight (theme)
    - vim-devicons
    - nerdtree (:NERDTree)
    - startify
    - coq_nvim (:COQnow)
    - vim-which-key
- configs for
    - starship
    - tmux
    - alacritty
    - neovim
