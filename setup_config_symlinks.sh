#!/bin/sh

SCRIPTS_DIR=`dirname "$(readlink -f "$0")"`
CONFIG_DIR="${SCRIPTS_DIR}/config"

# Starship
ln -fs ${CONFIG_DIR}/starship.toml ~/.config/starship.toml

# Tmux
ln -fs ${CONFIG_DIR}/tmux.conf ~/.tmux.conf
ln -fs ${CONFIG_DIR}/tmux.conf.local ~/.tmux.conf.local

# Alacritty
#rm -rf ~/.config/alacritty/
mkdir -p ~/.config/alacritty/
ln -fs ${CONFIG_DIR}/alacritty.yml ~/.config/alacritty/

# Neovim
rm -rf ~/.config/nvim
ln -fs ${CONFIG_DIR}/neovim/ ~/.config/nvim

# Polybar
rm -rf ~/.config/polybar
ln -fs ${CONFIG_DIR}/polybar/ ~/.config/polybar

# Kitty
mkdir -p ~/.config/kitty
ln -fs ${CONFIG_DIR}/kitty/kitty.conf ~/.config/kitty/

# i3
#mkdir -p ~/.config/i3
#ln -fs ${CONFIG_DIR}/i3/config ~/.config/i3/config
rm -rf ~/.config/i3
ln -fs ${CONFIG_DIR}/i3/ ~/.config/i3



## Hard-copy
sudo mkdir -p /etc/X11/xorg.conf.d/ 
sudo cp ${CONFIG_DIR}/X11/xorg.conf.d/90-touchpad.conf /etc/X11/xorg.conf.d/


