#!/bin/sh
set -x
 
REBOOT=false
SCRIPTS_DIR=`dirname "$(readlink -f "$0")"`

cd ~
 
# Some base packages
sudo apt --yes install curl zsh git tmux unzip fontconfig playerctl

# OH-MY-ZSH
sh ${SCRIPTS_DIR}/zsh_installation.sh

# Nerdfonts
sh ${SCRIPTS_DIR}/nerdfonts_installation.sh
 
# Starship
sh ${SCRIPTS_DIR}/starship_installation.sh

# Apps
## Neovim
sh ${SCRIPTS_DIR}/neovim_installation.sh

## Polybar
sh ${SCRIPTS_DIR}/polybar_installation.sh

## Kitty
sh ${SCRIPTS_DIR}/kitty_installation.sh

## brightnessctl
sh ${SCRIPTS_DIR}/brightnessctl_installation.sh

# Symbolic links
sh ${SCRIPTS_DIR}/setup_config_symlinks.sh



# DONE
echo "Setup done!"
 
# Reboot
if [ $REBOOT = true ]; then
  bash -c 'read -n 1 -s -r -p "Press enter to reboot machine..."'
  sudo reboot now
else
  echo "Restart terminal!"
fi
